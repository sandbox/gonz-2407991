<?php

/**
 * @file
 * Rules integration for proform.
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_condition_info().
 */
function commerce_vip_discount_rules_condition_info() {
  $conditions = array();

  $conditions['vip_user_has_vip_role'] = array(
    'label' => t('User has VIP role.'),
    'parameter' => array(
      'user' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
    ),
    'group' => t('VIP Discount'),
    'callbacks' => array(
      'execute' => 'vip_user_has_vip_role',
    ),
  );

  return $conditions;
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_vip_discount_rules_action_info() {
  $actions = array();

  $actions['vip_remove_user_role'] = array(
    'label' => t('Remove VIP role (update commerce_vip_discounts table)'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order to log'),
      ),
      'user' => array(
        'type' => 'user',
        'label' => t('User'),
      ),
    ),
    'group' => t('VIP Discount'),
    'callbacks' => array(
      'execute' => 'vip_remove_user_role',
    ),
  );

  return $actions;
}

function commerce_vip_discount_remove_user_role($order, $user) {
  db_update('commerce_vip_discounts')
    -> fields(array(
      'order_id' => $order->order_id,
    ))
    ->isNull('order_id')
    ->condition('uid', $order->uid, '=')
    ->execute();

  $role = user_role_load_by_name('vipcustomer');
  if (isset($user->roles[$role->rid])) {
    unset($user->roles[$role->rid]);
    user_save($user);
  }
}

/**
 * @}
 */
