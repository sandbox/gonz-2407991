<?php

/**
 * The form API.
 */
function commerce_vip_discount_generate_codes_form($form, &$form_state) {
  $form['code_quantity'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of Codes to generate'),
    '#required' => TRUE,
    '#default_value' => '',
    '#maxlength' => 4,
    '#size' => 4,
  );
  $form['code_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Length of Codes'),
    '#required' => TRUE,
    '#default_value' => '6',
    '#maxlength' => 2,
    '#size' => 2,
  );
  // Prepend
  $form['code_prepend'] = array(
    '#type' => 'textfield',
    '#title' => t('Prepend'),
    '#description' => t('The characters added before the code'),
    '#required' => FALSE,
    '#default_value' => '',
    '#size' => 8,
  );
  // Append
  $form['code_append'] = array(
    '#type' => 'textfield',
    '#title' => t('Append'),
    '#description' => t('The characters added after the code'),
    '#required' => FALSE,
    '#default_value' => '',
    '#size' => 8,
  );
  // Rep ID
  $form['code_rep'] = array(
    '#type' => 'textfield',
    '#title' => t('Rep ID'),
    '#description' => t('Sales rep data'),
    '#required' => FALSE,
    '#default_value' => '',
    '#size' => 8,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate Codes'),
  );

  return $form;
}

function commerce_vip_discount_generate_codes_form_validate($form, &$form_state) {
	// Get the Form Values
  	$quantity = $form_state['values']['code_quantity'];
  	if (!is_numeric($quantity)) {
  		form_set_error('code_quantity', t('Please use real numbers palzy'));
  	}
}

/**
 * Form submission logic for the contact form.
 */
function commerce_vip_discount_generate_codes_form_submit($form, &$form_state) {
	// Get the Form Values
  	$quantity = $form_state['values']['code_quantity'];
  	$length = $form_state['values']['code_length'];
  	$prepend = $form_state['values']['code_prepend'];
  	$append = $form_state['values']['code_append'];
  	$rep = $form_state['values']['code_rep'];


	//$test = generateUniqueCode('proform', $length, $prepend, $append);
  	// Update Proform Registration in DB 
  	for($i = 0; $i < $quantity; $i++) {
  		$code = generateUniqueCode('proform', $length, $prepend, $append);

  		// db_query("insert into couponcodes (code, batch_date, codeType) values('%s', '%s', '%s')", $code, $time, $type);
  		db_insert('proforms')
	    ->fields(array(
	      'code' => $code,
	      'used' => 0,
	      'date_time' => NULL,
	      'uid' => 0,
	      'batch_date' => REQUEST_TIME,
	      'code_type' => 'proform',
	      'order_id' => NULL,
	      'notes' => $rep,
	    ))
	    ->execute();
  	}

	drupal_set_message(t('Finished processing %string codes.', array('%string' => $quantity)), 'status');


}

//

function generateUniqueCode($type = proform, $digits = 6, $prepend = NULL, $append = NULL) {
	$rangeMax = str_pad(1, $digits, 0, STR_PAD_RIGHT);
	$rangeMin = str_pad(9, $digits, 9, STR_PAD_RIGHT);
	// Create a random number given the digit range
	$randval = $prepend . rand($rangeMax, $rangeMin) . $append;

	// Make sure it is unique
	if (!db_query("SELECT COUNT(code) FROM {proforms} WHERE code = :code;", array(':code' => $randval))->fetchField()) {
		return $randval;
	} else {
		// code was already used - get a new one
        return generateUniqueCode($type, $digits, $prepend, $append);
	}
}