<?php

/**
 * The form API.
 */
function commerce_vip_discount_loggedin_user_form($form, &$form_state) {
  global $user;
  $proformName = variable_get('proform_name_setting', 'Proform');

  $proforminfo = "You have now successfully registered for the Neff Proform.  You can now buy up to 7 items at 50% off.  Each proform code can only be used once regardless of how many items are purchased.  Start shopping!";

  $form['account'] = array(
    '#type' => 'value',
    '#value' => $user->uid,
  );
  $form['registration_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Registration Code'),
    '#required' => TRUE,
    '#default_value' => '',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Register {$proformName} Code"),
  );

  return $form;
}

/**
 * Form validation logic for the form.
 */
function commerce_vip_discount_loggedin_user_form_validate($form, &$form_state) {
  $account = $form_state['values']['account']; // get user->obj from form field static value
  // Check the DB for Code
  $proformcode = $form_state['values']['registration_code'];

  checkProformCode($proformcode);
}

/**
 * Form submission logic for the contact form.
 */
function commerce_vip_discount_loggedin_user_form_submit($form, &$form_state) {
  // Everything worked great - lets give them a role of Proform

  // Define Module Variables
  $proformName = variable_get('proform_name_setting', 'Proform');
  $successMessage = variable_get('proform_success_msg', 'You have successfully registered your VIP code.');

  // Get some Values from the Formn to store in the DB
  global $user;
  $proformcode = $form_state['values']['registration_code'];

  // Get the Role ID and Append to the User
  $role_name = 'proform'; // The name of the role to add.
  if ($role = user_role_load_by_name($role_name)) {
    user_multiple_role_edit(array($user->uid), 'add_role', $role->rid);
  }

  // Update Proform Registration in DB 
    db_update('proforms')
    -> fields(array(
        'date_time' => REQUEST_TIME,
        'used' => 1,
        'uid' => $user->uid,
    ))
    ->condition('code', $proformcode, '=')
    ->execute();

  // Success Message
  drupal_set_message(t("{$successMessage}"));

  drupal_goto('<front>');

  }
