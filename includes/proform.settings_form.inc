<?php

/**
 * @file
 * Administration pages provided by VIP (Proform) module.
 */

function commerce_vip_discount_settings_form($form, &$form_state) {
	// Define Module Variables
  	$proformName = variable_get('proform_name_setting', 'Proform');

	// VIP (Proform) Custom Name
	$form['proform_name_setting'] = array(
		'#type' => 'textfield',
		'#title' => t('Customize VIP Name'),
		'#default_value' => variable_get('proform_name_setting', 'Proform'), // This will retrieve Drupal's Module Stored Variables
		'#size' => 90,
		'#maxlength' => 255,
		'#description' => t('ex: Proform, VIP, Prefered Member, etc.'),
	);
	// User Message - Success
	$form['proform_success_msg'] = array(
		'#type' => 'textarea',
		'#title' => t('Success Message'),
		'#description' => t("Text shown to user after successfully registering their {$proformName} code."),
		'#rows' => 2,
		'#columns' => 20,
		'#default_value' => variable_get('proform_success_msg', 'You have successfully registered your VIP code.'), 
  	);
  	// User Message - Code Already Used
	$form['proform_used_code'] = array(
		'#type' => 'textarea',
		'#title' => t('Form Message - Code Already Used'),
		'#description' => t("Text shown to user if their code had already been registered. You can use tokens: '@code' and '@dateUsed' in your message."),
		'#rows' => 2,
		'#columns' => 20,
		'#default_value' => variable_get('proform_used_code', 'Bummer - the Code @code was used on @dateUsed!'), 
  	);
  	// User Message - Code was Invalid
	$form['proform_invalid_code'] = array(
		'#type' => 'textarea',
		'#title' => t('Form Message - Invalid Code'),
		'#description' => t("Text shown to user if their code was invalid."),
		'#rows' => 2,
		'#columns' => 20,
		'#default_value' => variable_get('proform_invalid_code', 'Bummer - the Registration Code is invalid. Please check your entry and try again.'), 
  	);

	// The submit function saves all the data in the form, using variable_set(), to variables named the same as the keys in the form array.
	return system_settings_form($form);
}