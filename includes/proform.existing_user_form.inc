<?php

/**
 * The form API.
 */
function commerce_vip_discount_existing_user_form($form, &$form_state) {
  // Define Module Variables
  $proformName = variable_get('proform_name_setting', 'Proform');

  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#required' => TRUE,
    '#default_value' => '',
  );
  $form['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#required' => TRUE,
    '#default_value' => '',
  );
  $form['registration_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Registration Code'),
    '#required' => TRUE,
    '#default_value' => '',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Register {$proformName} Code"),
  );

  return $form;
}

/**
 * Form validation logic for the form.
 */
function commerce_vip_discount_existing_user_form_validate($form, &$form_state) {
  // Get the Form Values
  $username = $form_state['values']['username'];
  $password = $form_state['values']['password'];
  $proformcode = $form_state['values']['registration_code'];

  if (user_authenticate($username, $password)) {
      checkProformCode($proformcode);

      // $user_obj = user_load_by_name($username);
      //drupal_set_message(t("You are logging in with User ID =  @user_id. Keep it up!", array('@user_id' => $user_obj->uid)));
  } else {
      // They did not enter in the correct info
      form_set_error('username', t('The username or password you have entered does not match our system.'));
  }
  
}


/**
 * Form submission logic for the contact form.
 */
function commerce_vip_discount_existing_user_form_submit($form, &$form_state) {
  // Everything worked great - lets give them a role of Proform

  // Define Module Variables
  $proformName = variable_get('proform_name_setting', 'Proform');
  $successMessage = variable_get('proform_success_msg', 'You have successfully registered your VIP code.');

  // Operate on the global $user variable so it is available to
  // user_login_finalize() when called.
  global $user;

  // Get the Form Values
  $username = $form_state['values']['username'];
  $password = $form_state['values']['password'];
  $proformcode = $form_state['values']['registration_code'];

  $existing_user = user_load_by_name($username);

  $user = user_load($existing_user->uid);

  // Get the Role ID and Append to the User
  $role_name = 'proform'; // The name of the role to add.
  if ($role = user_role_load_by_name($role_name)) {
    user_multiple_role_edit(array($existing_user->uid), 'add_role', $role->rid);
  }

  // Update the DB so we know it has been used
  db_update('proforms')
    -> fields(array(
        'date_time' => REQUEST_TIME,
        'used' => 1,
        'uid' => $existing_user->uid,
    ))
    ->condition('code', $proformcode, '=')
    ->execute();
  // Login the user and let them start buying mad proform goods
  drupal_session_regenerate();

  // Success Message
  drupal_set_message(t("{$successMessage}")); 

  drupal_goto('<front>');

}