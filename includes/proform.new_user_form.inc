<?php

/**
 * The form API.
 */
function commerce_vip_discount_new_user_form($form, &$form_state) {
  // Define Module Variables
  $proformName = variable_get('proform_name_setting', 'Proform');

  $form['registration_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Registration Code'),
    '#required' => TRUE,
    '#default_value' => '',
  );
  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#required' => TRUE,
    '#default_value' => '',
  );
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#required' => TRUE,
    '#default_value' => "",
    '#description' => "Your valid e-mail address. All e-mails from the system will be sent to this address. The e-mail address is not made public and will only be used if you wish to receive a new password or wish to receive certain news or notifications by e-mail.",
    '#size' => 60,
    '#maxlength' => 128,
  );
  $form['pass_fields'] = array(
    '#type' => 'password_confirm',
    '#description' => t('Enter the same password in both fields'),
    '#size' => 32,
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Register {$proformName} Code"),
  );

  return $form;
}

/**
 * Form validation logic for the form.
 */
function commerce_vip_discount_new_user_form_validate($form, &$form_state) {
  // Get the Form Values
  $username = $form_state['values']['username'];
  $proformcode = $form_state['values']['registration_code'];
  $email = $form_state['values']['email'];
  $password = $form_state['values']['pass_fields']['pass1'];

  checkProformCode($proformcode);

  // Check if the username is already taken
  if (!db_query("SELECT COUNT(*) FROM {users} WHERE name = :name;", array(':name' => $username))->fetchField()) {
    // User doesn't exist
    // drupal_set_message(t('The Username "%username" is Perfect!', array('%username' => $username)), 'status');
  } else {
    form_set_error('username', t('The Username "%username" is already taken - try something else!', array('%username' => $username)));
    return; // Return to the From - no further processing needed
  }

  

  // Check if the email is already taken
  if (!db_query("SELECT COUNT(*) FROM {users} WHERE mail = :mail;", array(':mail' => $email))->fetchField()) {
    // It appears the email is good to go...
    // Check if the Email is Valid - not SPAM
    if (!valid_email_address($email)) {
      form_set_error('email', t('The email address appears to be invalid.'));
    }
  } else {
    form_set_error('email', t('The Email "%email" is already taken - try something else!', array('%email' => $email)));
    return; // Return to the From - no further processing needed
  }

}

/**
 * Form submission logic for the contact form.
 */
function commerce_vip_discount_new_user_form_submit($form, &$form_state) {
  // Everything worked great - lets give them a role of Proform


  // Define Module Variables
  $proformName = variable_get('proform_name_setting', 'Proform');
  $successMessage = variable_get('proform_success_msg', 'You have successfully registered your VIP code.');

  // Operate on the global $user variable so it is available to
  // user_login_finalize() when called.
  global $user;

  // Get the Form Values
  $username = $form_state['values']['username'];
  $proformcode = $form_state['values']['registration_code'];
  $email = $form_state['values']['email'];
  $password = $form_state['values']['pass_fields']['pass1'];


  // Determine the roles of our new user
  // Role to grant the permissions to
  $proform_role = user_role_load_by_name('proform'); 
  $proform_rid = $proform_role->rid;
  $new_user_roles = array(
    DRUPAL_AUTHENTICATED_RID => 'authenticated user',
    $proform_rid => TRUE,
  );

  // Create a new user
  $new_user = new stdClass();
  $new_user->name = $username;
  $new_user->pass = $password; // plain text, hashed later
  $new_user->mail = $email;
  $new_user->roles = $new_user_roles;
  $new_user->status = 1; // omit this line to block this user at creation
  $new_user->is_new = TRUE; // not necessary because we already omit $new_user->uid
  $new_user->timezone = NULL;
  // Create New User
  $user = user_save($new_user);

  // Update Proform Registration in DB 
  
  // Success Message
  drupal_set_message(t("{$successMessage}"));

  // Login the new user
  
  user_login_finalize();

  db_update('proforms')
    -> fields(array(
        'date_time' => REQUEST_TIME,
        'used' => 1,
        'uid' => $new_user->uid,
    ))
    ->condition('code', $proformcode, '=')
    ->execute();

  drupal_goto('<front>');
  

}
