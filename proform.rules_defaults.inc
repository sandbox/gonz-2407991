<?php

/**
 * @file
 * Default rule configurations for Proform.
 */


/**
 * Implements hook_default_rules_configuration().
 */
function commerce_vip_discount_default_rules_configuration() {
  $rules = array();

  // Proform Discount
  $rule = rules_reaction_rule();

  $rule->label = t('VIP Discount Price');
  $rule->tags = array('VIP Discount');
  $rule->active = TRUE;

  $rule
    ->event('commerce_product_calculate_sell_price')
    ->condition('vip_user_has_vip_role', array(
      'user' => array('[site:current-user]')
    ))
    ->action('commerce_line_item_unit_price_multiply', array(
      'commerce_line_item' => array('commerce-line-item'),
      'amount' => '0.5',
      'component_name' => 'commerce_coupon|commerce_coupon_fixed',
      'round_mode' => '1',
    ));

  $rules['rules_vip_discount_price'] = $rule;

  // Proform Remove Role
  $rule = rules_reaction_rule();

  $rule->label = t('VIP remove role');
  $rule->tags = array('VIP Discount');
  $rule->active = TRUE;

  $rule
    ->event('commerce_checkout_complete')
    ->condition('vip_user_has_vip_role', array(
      'user' => array('[site:current-user]')
    ))
    ->action('vip_remove_user_role', array(
      'commerce_order' => array('commerce_order'),
      'user' => array('[site:current-user]')
    ));

  $rules['rules_vip_remove_role'] = $rule;

  // Proform No Coupons
  $rule = rules_reaction_rule();

  $rule->label = t('Proform - no coupons');
  $rule->tags = array('Proform');
  $rule->active = TRUE;

  $rule
    ->event('commerce_coupon_validate')
    ->condition('vip_user_has_vip_role', array(
      'user' => array('[site:current-user]')
    ))
    ->action('drupal_message', array(
      'message' => t('Coupons are not applicable with Proform discounts.'),
    ))
    ->action('commerce_coupon_action_is_invalid_coupon', array());

  $rules['rules_vip_no_coupons'] = $rule;

  return $rules;
}

